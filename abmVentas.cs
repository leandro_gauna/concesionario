﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Negocios;

namespace Concesionario
{
    public partial class abmVentas : Form
    {
        public abmVentas()
        {
            InitializeComponent();
        }

        private void abmVentas_Activated(object sender, EventArgs e)
        {
            Negocios.auto.ListarTodos();
            Negocios.cliente.ListarTodos();
        }
        private void Enlazar()
        {
            dgvAuto.DataSource = null;
            dgvAuto.DataSource = Negocios.auto.ListarTodos();
            dgvCliente.DataSource = null;
            dgvCliente.DataSource = Negocios.cliente.ListarTodos();
            dgvVentas.DataSource = null;
            dgvVentas.DataSource = Negocios.Venta.Listar();
        }
        private void abmVentas_Load(object sender, EventArgs e)
        {
            Enlazar();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnVenta_Click(object sender, EventArgs e)
        {
            Venta unaVenta = new Venta();
            unaVenta.Insertar(dtFecha.Value, (cliente)dgvCliente.SelectedRows[0].DataBoundItem, (auto)dgvAuto.SelectedRows[0].DataBoundItem); ;
            Enlazar();
        }
    }
}
