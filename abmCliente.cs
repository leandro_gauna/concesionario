﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Negocios;

namespace Concesionario
{
    public partial class abmCliente : Form
    {
        public abmCliente()
        {
            InitializeComponent();
        }

        public void Enlazar()
        {
            dgvCliente.DataSource = null;
            dgvCliente.DataSource = Negocios.cliente.ListarTodos();
        }

        private void btnAlta_Click(object sender, EventArgs e)
        {
            cliente unCliente = new cliente();
            unCliente.Insertar(txtNombre.Text, txtApellido.Text,int.Parse(txtDNI.Text));

            Enlazar();
        }

        private void btnMod_Click(object sender, EventArgs e)
        {
            cliente unCliente = (cliente)dgvCliente.SelectedRows[0].DataBoundItem;
            unCliente.Modificar(txtNombre.Text, txtApellido.Text, int.Parse(txtDNI.Text));
            Enlazar();
        }

        private void btnBaja_Click(object sender, EventArgs e)
        {
            cliente unCliente = (cliente)dgvCliente.SelectedRows[0].DataBoundItem;
            unCliente.Borrar();
            Enlazar();
        }

        private void abmCliente_Load(object sender, EventArgs e)
        {
            Enlazar();
        }
    }
}
