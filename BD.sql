USE [master]
GO
/****** Object:  Database [Concesionaria]    Script Date: 7/9/2020 19:48:45 ******/
CREATE DATABASE [Concesionaria]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Concesionaria', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\Concesionaria.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Concesionaria_log', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\Concesionaria_log.ldf' , SIZE = 2048KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [Concesionaria] SET COMPATIBILITY_LEVEL = 90
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Concesionaria].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Concesionaria] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Concesionaria] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Concesionaria] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Concesionaria] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Concesionaria] SET ARITHABORT OFF 
GO
ALTER DATABASE [Concesionaria] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Concesionaria] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [Concesionaria] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Concesionaria] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Concesionaria] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Concesionaria] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Concesionaria] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Concesionaria] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Concesionaria] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Concesionaria] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Concesionaria] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Concesionaria] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Concesionaria] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Concesionaria] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Concesionaria] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Concesionaria] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Concesionaria] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Concesionaria] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Concesionaria] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Concesionaria] SET  MULTI_USER 
GO
ALTER DATABASE [Concesionaria] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Concesionaria] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Concesionaria] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Concesionaria] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [Concesionaria]
GO
/****** Object:  StoredProcedure [dbo].[AUTO_Borrar]    Script Date: 7/9/2020 19:48:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROC [dbo].[AUTO_Borrar] 
@id_auto int 


as
BEGIN
delete auto where id_auto = @id_auto
 
END
GO
/****** Object:  StoredProcedure [dbo].[AUTO_EDITAR]    Script Date: 7/9/2020 19:48:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROC [dbo].[AUTO_EDITAR] 
@id_auto int, 
@marca varchar(50),
@modelo varchar(50),
@patente varchar(50),
@precio float,
@anio int

as
BEGIN
update auto set
marca = @marca,
modelo = @modelo,
patente = @patente,
precio = @precio,
anio = @anio
where id_auto = @id_auto
 
END
GO
/****** Object:  StoredProcedure [dbo].[AUTO_INSERTAR]    Script Date: 7/9/2020 19:48:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[AUTO_INSERTAR] 
@marca varchar(50),
@modelo varchar(50),
@patente varchar(50),
@precio float,
@anio int


as
BEGIN

Declare @id_auto int -- declare que es entero

set @id_auto  = (SELECT ISNULL ( MAX(id_auto) , 0) +1 FROM auto) 
--agregamos el select de obtener ID todo entre parentesis.


insert into auto values(@id_auto,@marca,@modelo,@patente,@precio, @anio)
 
END


GO
/****** Object:  StoredProcedure [dbo].[AUTO_Listar]    Script Date: 7/9/2020 19:48:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[AUTO_Listar]
as 
begin
select * from AUTO
end

GO
/****** Object:  StoredProcedure [dbo].[CLIENTE_Borrar]    Script Date: 7/9/2020 19:48:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROC [dbo].[CLIENTE_Borrar] 
@id_cliente int 


as
BEGIN
delete cliente where id_cliente = @id_cliente
 
END
GO
/****** Object:  StoredProcedure [dbo].[CLIENTE_EDITAR]    Script Date: 7/9/2020 19:48:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROC [dbo].[CLIENTE_EDITAR] 
@id_cliente int, 
@nombre varchar(50),
@apellido varchar(50),
@dni int

as
BEGIN
update cliente set
nombre = @nombre,
apellido = @apellido,
dni = @dni
where id_cliente = @id_cliente
 
END
GO
/****** Object:  StoredProcedure [dbo].[CLIENTE_INSERTAR]    Script Date: 7/9/2020 19:48:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[CLIENTE_INSERTAR] 
@nombre varchar(50),
@apellido varchar(50),
@dni int


as
BEGIN

Declare @id_cliente int -- declare que es entero

set @id_cliente  = (SELECT ISNULL ( MAX(id_cliente) , 0) +1 FROM cliente) 
--agregamos el select de obtener ID todo entre parentesis.


insert into cliente values(@id_cliente, @nombre, @apellido, @dni)
 
END
GO
/****** Object:  StoredProcedure [dbo].[CLIENTE_Listar]    Script Date: 7/9/2020 19:48:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[CLIENTE_Listar]
as 
begin
select * from CLIENTE
end
GO
/****** Object:  StoredProcedure [dbo].[OBTENER_ID]    Script Date: 7/9/2020 19:48:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create proc [dbo].[OBTENER_ID]
as 
begin
SELECT ISNULL ( MAX(id_auto) , 0) +1 FROM auto

end
GO
/****** Object:  StoredProcedure [dbo].[VENTA_Borrar]    Script Date: 7/9/2020 19:48:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[VENTA_Borrar] 
@id_venta int 


as
BEGIN
delete venta where id_venta = @id_venta
 
END
GO
/****** Object:  StoredProcedure [dbo].[VENTA_INSERTAR]    Script Date: 7/9/2020 19:48:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROC [dbo].[VENTA_INSERTAR] 
@fecha date,
@id_cliente int,
@id_auto int

as
BEGIN

Declare @id_venta int -- declare que es entero

set @id_venta  = (SELECT ISNULL ( MAX(id_venta) , 0) +1 FROM venta) 
--agregamos el select de obtener ID todo entre parentesis.


insert into venta values(@id_venta,@fecha,@id_cliente,@id_auto)
 
END
GO
/****** Object:  StoredProcedure [dbo].[venta_Listar]    Script Date: 7/9/2020 19:48:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[venta_Listar]
as 
begin
select * from venta
end
GO
/****** Object:  Table [dbo].[auto]    Script Date: 7/9/2020 19:48:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[auto](
	[id_auto] [int] NOT NULL,
	[marca] [varchar](50) NOT NULL,
	[modelo] [varchar](50) NOT NULL,
	[patente] [varchar](50) NOT NULL,
	[precio] [float] NOT NULL,
	[anio] [int] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[cliente]    Script Date: 7/9/2020 19:48:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cliente](
	[id_cliente] [int] NOT NULL,
	[nombre] [varchar](50) NOT NULL,
	[apellido] [varchar](50) NOT NULL,
	[dni] [int] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[venta]    Script Date: 7/9/2020 19:48:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[venta](
	[id_venta] [int] NOT NULL,
	[fecha] [date] NOT NULL,
	[id_cliente] [int] NOT NULL,
	[id_auto] [int] NOT NULL
) ON [PRIMARY]

GO
USE [master]
GO
ALTER DATABASE [Concesionaria] SET  READ_WRITE 
GO
