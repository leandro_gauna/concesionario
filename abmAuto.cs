﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Negocios;
using System.Data.SqlClient;


namespace Concesionario
{
    public partial class abmAuto : Form
    {
        Acceso acceso = new Acceso();
        public abmAuto()
        {
            InitializeComponent();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Enlazar()
        {
            dgvAutos.DataSource = null;
            dgvAutos.DataSource = Negocios.auto.ListarTodos();
        }

        private void abmAuto_Load(object sender, EventArgs e)
        {
            Enlazar();
        }

        private void btnAlta_Click(object sender, EventArgs e)
        {
            #region
            //List<SqlParameter> parametros = new List<SqlParameter>();
            //Acceso acceso = new Acceso();
            //acceso.Abrir();
            ////parametros.Add(acceso.CrearParametro("@id_auto", 2));
            //parametros.Add(acceso.CrearParametro("@marca", txtMarca.Text));
            //parametros.Add(acceso.CrearParametro("@modelo", txtModelo.Text));
            //parametros.Add(acceso.CrearParametro("@patente", txtPatente.Text));
            //parametros.Add(acceso.CrearParametro("@precio", float.Parse(txtPrecio.Text).ToString()));
            //parametros.Add(acceso.CrearParametro("@anio", int.Parse(txtAnio.Text).ToString()));

            //acceso.Escribir("AUTO_INSERTAR", parametros);
            //acceso.Cerrar();
            #endregion
            auto unAuto = new auto();
            unAuto.Insertar(txtMarca.Text, txtModelo.Text, txtPatente.Text, float.Parse(txtPrecio.Text),int.Parse(txtAnio.Text));
            
            Enlazar();
            
        }

        private void dgvAutos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnMod_Click(object sender, EventArgs e)
        {
            auto unAuto = (auto)dgvAutos.SelectedRows[0].DataBoundItem;
            unAuto.Modificar(txtMarca.Text, txtModelo.Text, txtPatente.Text, float.Parse(txtPrecio.Text), int.Parse(txtAnio.Text));
            Enlazar();

        }

        private void btnBaja_Click(object sender, EventArgs e)
        {
            auto unAuto = (auto)dgvAutos.SelectedRows[0].DataBoundItem;
            unAuto.Borrar();
            Enlazar();
        }
    }
}
